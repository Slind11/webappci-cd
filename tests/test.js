const request = require('supertest');
const app = require('../server.js');

describe('GET /', () => {
  it('responds with "tests ci-cd"', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .expect('tests ci-cd', done);
  });
});
