const request = require('supertest');
const app = require('../server.js');

describe('GET /', function() {
  it('responds with "tests ci-cd"', function(done) {
    request(app)
      .get('/')
      .expect(200)
      .expect('tests ci-cd')
      .end(done);  // Передаем done для завершения теста
  });
});

